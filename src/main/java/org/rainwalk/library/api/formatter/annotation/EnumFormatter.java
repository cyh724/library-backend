package org.rainwalk.library.api.formatter.annotation;


import org.rainwalk.library.model.enums.DescValueEnum;

import java.lang.annotation.*;

/**
 * 枚举转换
 *
 * @author chenyuheng create class on 2021-02-20
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(EnumFormatters.class)
public @interface EnumFormatter {

    String field();

    Class<? extends DescValueEnum> enumClazz();

    String suffix() default "Formatter";
}
