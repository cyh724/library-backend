package org.rainwalk.library.api.auth;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 权限配置属性
 *
 * @author 趁雨行 2021-07-22 17:06:05
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "auth")
public class AuthProperties {

    private String key = "rainwalk";

    private String username = "rainwalk";

    private String password = "rainwalk";
}
