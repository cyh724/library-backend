package org.rainwalk.library.mapper;

import org.rainwalk.library.entity.BrowseLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件浏览记录 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
public interface BrowseLogMapper extends BaseMapper<BrowseLog> {

}
