package org.rainwalk.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.rainwalk.library.entity.Storage;

import java.util.List;

/**
 * <p>
 * 存储项目 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
public interface StorageMapper extends BaseMapper<Storage> {

    /**
     * 检查父目录存在并插入
     *
     * @param storage 存储信息
     * @return 影响条数
     */
    int insertWhenPidExists(Storage storage);

    /**
     * 修改文件父目录
     *
     * @param storage 存储信息
     * @return
     */
    int updatePid(Storage storage);

    /**
     * 修改文件名
     *
     * @param id 存储信息
     * @param title 新标题
     * @return
     */
    int updateTitle(@Param("id") Long id,@Param("title") String title);

    /**
     * 查询已删除的文件
     *
     * @return
     */
    List<Storage> deletedFileList();

    /**
     * 更新为es未写入状态
     *
     * @param id
     */
    void undoEsById(@Param("id") Long id);
}
