package org.rainwalk.library.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import org.rainwalk.library.entity.Storage;
import org.rainwalk.library.service.ILibraryService;
import org.rainwalk.library.service.IStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 文件定时任务
 *
 * @author 趁雨行 2021/7/9
 */
@Component
@RequiredArgsConstructor(onConstructor_={@Autowired})
public class StorageTask {

    private final IStorageService storageService;

    private final ILibraryService libraryService;

    @Scheduled(cron = "0 0 0 * * ? *")
    public void write() {
        LambdaQueryWrapper<Storage> storageLambdaQueryWrapper = new LambdaQueryWrapper<>();
        storageLambdaQueryWrapper.eq(Storage::getEsWrite, false)
                .eq(Storage::getIsDirectory, false);
        List<Storage> list = storageService.list(storageLambdaQueryWrapper);
        for (Storage storage : list) {
            libraryService.write(storage);
        }
    }

    @Scheduled(cron = "0 0 0 * * ? *")
    public void clean() {
        List<Storage> list = storageService.deletedFileList();
        for (Storage storage : list) {
            libraryService.deleteById(storage);
        }
    }



}
