package org.rainwalk.library.model.dto;

import lombok.Data;

/**
 * 分页参数
 *
 * @author 趁雨行 2021/7/9
 */
@Data
public class PageDTO {

    private Integer pageNum = 1;

    private Integer pageSize = 10;

}
