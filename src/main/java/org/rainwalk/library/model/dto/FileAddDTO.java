package org.rainwalk.library.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 上传文件
 *
 * @author 趁雨行 2021/7/7
 */
@Data
public class FileAddDTO {

    @NotBlank(message = "请提供文件标题")
    @Size(max = 50, message = "文件标题长度超过 50！")
    private String title;

    @NotNull(message = "请提供文件")
    private MultipartFile file;

    @NotNull(message = "请提供上级目录")
    private Long pid;

}
