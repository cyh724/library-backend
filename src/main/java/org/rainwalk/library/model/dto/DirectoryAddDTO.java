package org.rainwalk.library.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 新增目录
 *
 * @author 趁雨行 2021/7/6
 */
@Data
public class DirectoryAddDTO {

    @NotBlank(message = "请提供目录名称")
    private String title;

    private Long pid;
}
