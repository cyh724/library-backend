package org.rainwalk.library.model.vo;

import lombok.Data;

import java.util.List;

/**
 * 路径导航响应
 *
 * @author 趁雨行 2021/7/13
 */
@Data
public class RouteVO {

    private DirectoryVO route;

    private List<StorageVO> children;
}
