package org.rainwalk.library.model.vo;

import lombok.Data;

/**
 * 目录VO
 *
 * @author 趁雨行 2021/7/13
 */
@Data
public class DirectoryVO {

    private Long id;

    private String title;

    private DirectoryVO child;

}
