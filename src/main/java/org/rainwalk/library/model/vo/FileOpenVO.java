package org.rainwalk.library.model.vo;

import lombok.Data;

import java.io.File;

/**
 * 打开文件
 *
 * @author 趁雨行 2021/7/8
 */
@Data
public class FileOpenVO {

    private String title;

    private String type;

    private File file;
}
