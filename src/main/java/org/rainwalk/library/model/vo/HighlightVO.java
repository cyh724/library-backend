package org.rainwalk.library.model.vo;

import lombok.Data;

/**
 * 高亮结构
 *
 * @author 趁雨行 2021-07-19 14:56:36
 */
@Data
public class HighlightVO {

    private String highlight;

    private String subNo;
}
