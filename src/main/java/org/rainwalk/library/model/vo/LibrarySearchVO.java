package org.rainwalk.library.model.vo;

import lombok.Data;

import java.util.List;

/**
 * 全文检索响应结构
 *
 * @author 趁雨行 2021/7/9
 */
@Data
public class LibrarySearchVO {

    private Long id;

    private String title;

    private String fileType;

    private List<HighlightVO> highlights;

}
