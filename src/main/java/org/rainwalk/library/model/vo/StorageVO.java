package org.rainwalk.library.model.vo;

import lombok.Data;

/**
 * 存储项
 *
 * @author 趁雨行 2021/7/6
 */
@Data
public class StorageVO {

    private Long id;

    private String title;

    private Boolean directoryFlag;

    private String fileType;
}
