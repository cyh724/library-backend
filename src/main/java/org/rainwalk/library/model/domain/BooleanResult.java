package org.rainwalk.library.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * boolean类型响应增强
 *
 * @author 趁雨行 2021-07-16 23:14:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BooleanResult<T> {

    /**
     * boolean值
     */
    private boolean bool;

    /**
     * 附加信息
     */
    private T info;

}
