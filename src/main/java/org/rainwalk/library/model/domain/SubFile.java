package org.rainwalk.library.model.domain;

import lombok.Data;

import java.io.File;

/**
 * 子文件结构
 *
 * @author 趁雨行 2021-07-16 23:12:50
 */
@Data
public class SubFile {

    private File file;

    private String subNo;
}
