package org.rainwalk.library.model.enums;

import lombok.Getter;

/**
 * 响应类型
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Getter
public enum ContentType implements DescValueEnum {

    PDF(FileType.PDF, "application/pdf"),
    MARKDOWN(FileType.MARKDOWN, "application/octet-stream"),
//    TXT(FileType.TXT, "text/plain"),
    ;

    private FileType desc;

    private String value;

    ContentType(FileType desc, String value) {
        this.desc = desc;
        this.value = value;
    }

    public static ContentType getByType(FileType desc) {
        if (desc == null) {
            return null;
        }
        for (ContentType value : ContentType.values()) {
            if (value.desc.equals(desc)) {
                return value;
            }
        }
        return null;
    }

    public static ContentType getByType(String type) {
        if (type == null) {
            return null;
        }
        FileType fileType = FileType.getByValue(type);
        if (fileType == null) {
            return null;
        }
        for (ContentType value : ContentType.values()) {
            if (value.desc.equals(fileType)) {
                return value;
            }
        }
        return null;
    }

}
