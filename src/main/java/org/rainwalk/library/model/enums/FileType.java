package org.rainwalk.library.model.enums;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;

/**
 * 账单交易状态
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Getter
public enum FileType implements DescValueEnum {

    PDF("pdf", "pdf"),
    MARKDOWN("md", "md"),
//    TXT("txt", "txt"),
    ;

    private String desc;

    private String value;

    FileType(String desc, String value) {
        this.desc = desc;
        this.value = value;
    }

    public static FileType getByDesc(String desc) {
        if (StrUtil.isBlank(desc)) {
            return null;
        }
        for (FileType value : FileType.values()) {
            if (value.desc.equals(desc)) {
                return value;
            }
        }
        return null;
    }

    public static FileType getByValue(String value) {
        if (StrUtil.isBlank(value)) {
            return null;
        }
        for (FileType fileType : FileType.values()) {
            if (fileType.value.equals(value)) {
                return fileType;
            }
        }
        return null;
    }

}
