package org.rainwalk.library.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 文件浏览记录 前端控制器
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
@RestController
@RequestMapping("/library/browse-log")
public class BrowseLogController {

}

