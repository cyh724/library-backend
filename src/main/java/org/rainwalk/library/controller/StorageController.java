package org.rainwalk.library.controller;


import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import org.rainwalk.library.api.auth.Auth;
import org.rainwalk.library.api.log.ApiLog;
import org.rainwalk.library.api.model.Response;
import org.rainwalk.library.api.model.ResponseCode;
import org.rainwalk.library.model.vo.StorageVO;
import org.rainwalk.library.service.IStorageService;
import org.rainwalk.library.task.StorageTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 存储项目 前端控制器
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
@RestController
@RequestMapping("/storage")
@RequiredArgsConstructor(onConstructor_={@Autowired})
public class StorageController {

    private final IStorageService storageService;

    private final StorageTask storageTask;

    @GetMapping
    public Response find(@RequestParam String title) {
        if (StrUtil.isBlank(title)) {
            return Response.fail(ResponseCode.PARAMS_VERITY_FAIL, "title不可为空");
        }
        List<StorageVO> vos = storageService.findByTitle(title);
        return Response.success(vos);
    }

    @Auth
    @ApiLog("移动单个文件")
    @PatchMapping("/{id}")
    public Response move(@PathVariable Long id, @RequestParam Long pid) {
        storageService.move(id, pid);
        return Response.success("移动成功");
    }

    @Auth
    @ApiLog("移动多个文件")
    @PatchMapping
    public Response move(@RequestParam String ids, @RequestParam Long pid) {
        if (StrUtil.isBlank(ids)) {
            return Response.fail(ResponseCode.PARAMS_VERITY_FAIL, "请选择要移动的文件");
        }
        List<Long> longIds = Arrays.stream(ids.split(","))
                .map(Long::new)
                .collect(Collectors.toList());
        storageService.move(longIds, pid);
        return Response.success("移动成功");
    }

    @Auth
    @ApiLog("修改文件名")
    @PutMapping("/{id}")
    public Response edit(@PathVariable Long id, @RequestParam String title) {
        if (StrUtil.isBlank(title)) {
            return Response.fail(ResponseCode.PARAMS_VERITY_FAIL, "标题不能为空");
        }
        if (title.length() > 50) {
            return Response.fail(ResponseCode.PARAMS_VERITY_FAIL, "标题长度超过 50！");
        }
        storageService.edit(id, title);
        return Response.success("修改成功");
    }

    @Auth
    @ApiLog("删除文件")
    @DeleteMapping("/{id}")
    public Response delete(@PathVariable Long id) {
        storageService.delete(id);
        storageTask.clean();
        return Response.success("删除成功");
    }

    @Auth
    @ApiLog("批量删除文件")
    @DeleteMapping
    public Response delete(@RequestParam String ids) {
        if (StrUtil.isBlank(ids)) {
            return Response.fail(ResponseCode.PARAMS_VERITY_FAIL, "请选择要删除的文件");
        }
        List<Long> longIds = Arrays.stream(ids.split(","))
                .map(Long::new)
                .collect(Collectors.toList());
        storageService.delete(longIds);
        storageTask.clean();
        return Response.success("删除成功");
    }

}

