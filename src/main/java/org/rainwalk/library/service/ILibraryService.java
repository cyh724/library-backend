package org.rainwalk.library.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.rainwalk.library.entity.Storage;
import org.rainwalk.library.model.vo.LibrarySearchVO;

import java.io.IOException;
import java.util.List;

/**
 * es library索引服务
 *
 * @author 趁雨行 2021/7/8
 */
public interface ILibraryService {
    /**
     * 文件索引
     *
     * @param storage 文件信息
     */
    void write(Storage storage);

    /**
     * 删除storage文档
     *
     * @param storage
     */
    void deleteById(Storage storage);

    /**
     * 分页查询
     *  @param keyword 查询关键字
     * @param pageNum 页码
     * @param pageSize 页大小
     * @return
     */
    Page<LibrarySearchVO> searchPage(String keyword, Integer pageNum, Integer pageSize) throws IOException;

    /**
     * 查询
     *  @param keyword 查询关键字
     * @return
     */
    List<LibrarySearchVO> search(String keyword) throws IOException;
}
