package org.rainwalk.library.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 * markdown文件类型服务
 *
 * @author 趁雨行 2021-07-23 17:53:44
 */
@Slf4j
@Service
@ConfigurationProperties(prefix = "upload-file.md")
public class FileService4MdImpl extends FileServiceAbstract {

}
