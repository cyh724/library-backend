package org.rainwalk.library.service.impl;

import org.rainwalk.library.entity.BrowseLog;
import org.rainwalk.library.mapper.BrowseLogMapper;
import org.rainwalk.library.service.IBrowseLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文件浏览记录 服务实现类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
@Service
public class BrowseLogServiceImpl extends ServiceImpl<BrowseLogMapper, BrowseLog> implements IBrowseLogService {

}
