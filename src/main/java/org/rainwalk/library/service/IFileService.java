package org.rainwalk.library.service;

import org.rainwalk.library.model.domain.BooleanResult;
import org.rainwalk.library.model.domain.SubFile;
import org.rainwalk.library.model.enums.FileType;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * 文件服务接口
 *
 * @author 趁雨行 2021-07-16 21:20:01
 */
public interface IFileService {

    /**
     * 是否支持该文件类型
     *
     * @param fileType 文件类型
     * @return true if support
     */
    BooleanResult<String> support(FileType fileType);

    /**
     * 检查文件是否可以解析
     *
     * @param file 文件
     * @return true if can be parsed
     */
    BooleanResult<String> canParse(File file);

    /**
     * 将二进制文件保存到磁盘
     *
     * @param multipartFile 二进制文件
     * @return 磁盘上的文件
     * @throws IOException 文件写入磁盘失败
     */
    File saveToDisk(MultipartFile multipartFile) throws IOException;

    /**
     * 拆分文件
     *
     * @param file
     * @return 子文件列表
     * @throws IOException 文件拆分失败
     */
    List<SubFile> splitFile(File file) throws IOException;

    /**
     * 清除拆分文件时可能产生的资源
     *
     * @param file 主文件
     * @param subFiles 子文件列表
     */
    void cleanResourceOnSplitFile(File file, List<SubFile> subFiles);

    /**
     * 根据文件名获取文件
     *
     * @param filename
     * @return
     */
    Optional<File> fetchFile(String filename);

}
