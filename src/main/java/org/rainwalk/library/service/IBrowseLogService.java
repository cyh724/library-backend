package org.rainwalk.library.service;

import org.rainwalk.library.entity.BrowseLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件浏览记录 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
public interface IBrowseLogService extends IService<BrowseLog> {

}
