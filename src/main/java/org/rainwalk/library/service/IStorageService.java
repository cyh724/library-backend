package org.rainwalk.library.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.rainwalk.library.entity.Storage;
import org.rainwalk.library.model.dto.DirectoryAddDTO;
import org.rainwalk.library.model.dto.FileAddDTO;
import org.rainwalk.library.model.enums.FileType;
import org.rainwalk.library.model.vo.DirectoryVO;
import org.rainwalk.library.model.vo.FileOpenVO;
import org.rainwalk.library.model.vo.StorageVO;

import java.util.List;

/**
 * <p>
 * 存储项目 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
public interface IStorageService extends IService<Storage> {

    /**
     * 新建目录
     *
     * @param directoryAddDTO 目录信息
     * @return 目录信息
     */
    StorageVO addDirectory(DirectoryAddDTO directoryAddDTO);

    /**
     * 新建文件
     *
     * @param fileAddDTO 文件信息
     * @param fileType 文件类型
     * @return 文件信息
     */
    StorageVO addFile(FileAddDTO fileAddDTO, FileType fileType);

    /**
     * 获取目录内容
     *
     * @param id 目录id
     * @param onlyDirectory 是否只查询目录
     * @return 目录内容列表
     */
    List<StorageVO> toDirectory(Long id, boolean onlyDirectory);

    /**
     * 通过标题查找
     *
     * @param title 标题
     * @return 存储项列表
     */
    List<StorageVO> findByTitle(String title);

    /**
     * 移动文件
     * @param id 文件id
     * @param pid 目标父文件id
     * @return
     */
    void move(Long id, Long pid);

    /**
     * 批量移动文件
     * @param ids 文件id集合
     * @param pid 目标父文件id
     * @return
     */
    void move(List<Long> ids, Long pid);

    /**
     * 获取文件
     *
     * @param id
     * @return
     */
    FileOpenVO getFile(Long id);

    /**
     * 删除文件
     *
     * @param id 文件id
     */
    void delete(Long id);

    /**
     * 批量删除
     *
     * @param ids 文件id集合
     */
    void delete(List<Long> ids);

    /**
     * 查询已删除的文件
     *
     * @return
     */
    List<Storage> deletedFileList();

    /**
     * 获取目录路径
     *
     * @param id
     * @return
     */
    DirectoryVO getRoute(Long id);


    /**
     * 修改文件名
     *
     * @param id 文件id
     * @param title 新标题
     */
    void edit(Long id, String title);
}
