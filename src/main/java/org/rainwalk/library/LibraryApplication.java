package org.rainwalk.library;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * springboot应用启动类
 *
 * @author 趁雨行 2021/7/6
 */
@EnableAsync
@SpringBootApplication
@MapperScan("org.rainwalk.library.mapper")
@EnableElasticsearchRepositories("org.rainwalk.library.repository")
public class LibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(LibraryApplication.class, args);
    }

}
