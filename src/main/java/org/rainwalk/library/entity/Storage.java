package org.rainwalk.library.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 存储项目
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Storage implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 父级id，顶级默认为0
     */
    private Long pid;

    /**
     * 标题
     */
    private String title;

    /**
     * 是否目录 1-目录 0-文件
     */
    private Boolean isDirectory;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 文件相对物理存储路径
     */
    private String relativePath;

    /**
     * 是否写入es 0-未写入 1-写入 null-无需写入
     */
    private Boolean esWrite;

    /**
     * 创建时间戳
     */
    private Integer createTime;

    /**
     * 是否删除 0-未删除 1-已删除
     */
    @TableLogic
    private Boolean deleted;


}
