package org.rainwalk.library.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 文件浏览记录
 * </p>
 *
 * @author 趁雨行
 * @since 2021-07-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BrowseLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 存储项目id
     */
    private Long storageId;

    /**
     * 用户ip
     */
    private String ip;

    /**
     * 打开时间
     */
    private Integer logTime;


}
