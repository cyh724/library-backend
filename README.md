## 个人书室-后端

### 介绍

一款用于管理、阅读图书的web应用，方便个人可以在任何可联网机器上查询自己的图书

目前支持图书格式：pdf、markdown

### 技术选型

- springboot 2.5.2

- mysql 8.0.25

- mybatis-plus 3.4.2

- elasticsearch 7.13.2

- hutool 5.7.5

- pdfbox 3.0.0-RC1

  ...

### 项目预览

![搜索文件](https://images.gitee.com/uploads/images/2021/0727/165846_77db6a26_3028238.gif "搜索.gif")


![文件操作](https://images.gitee.com/uploads/images/2021/0727/165910_7ea12118_3028238.gif "文件操作.gif")


![阅读文件](https://images.gitee.com/uploads/images/2021/0727/170356_e1753ccd_3028238.gif "阅读文件4.gif")